---
layout: page
title: Qui Som?
permalink: /quisom/
---

Code N' Semicolon es un empresa fundada en 1996 per un grup de professionals apasionats per les noves tecnologies. El nostre objectiu es desenvolupar apliacacions per a dispositius mòbils, a demés de
pàgines web a partir de gestors de continguts destinat a les empreses.

You can find the source code for the Jekyll new theme at:
{% include icon-github.html username="jglovier" %} /
[jekyll-new](https://github.com/jglovier/jekyll-new)

You can find the source code for Jekyll at
{% include icon-github.html username="jekyll" %} /
[jekyll](https://github.com/jekyll/jekyll)
