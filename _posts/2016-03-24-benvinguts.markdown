---
layout: post
title:  "Benvinguts!"
date:   2016-03-24 15:32:14 -0300
categories: jekyll update
---
Benvinguts a Code N' Semicolon, som una empresa dedicada al desenvolupament d'aplicacions tant per a dispositius mòbils, i al desenvolupament de pàgines web a partir de gestors de continguts
i portals d'aprenentatge. El nostre objectiu es garantir les necessitats dels nostres clients i proporcionar-lis un bon servei gràcies a la nostre experiencia des de fa més de 15 anys.